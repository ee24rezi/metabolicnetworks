from Bio import SeqIO
from pulp import *
from functions_for_wp1 import *
from compounds import amino_acid_dict


def convert_letter(letter):
    """
    function takes a capital letter and gives back the corresponding
    Amino Acid.
    :param letter:
    :return: amino-acid name
    """
    name = amino_acid_dict[letter]['name']
    return name


def read_protein_data(list_of_species=None):
    """
    function to read from Proteom data (folder)

    :return: list proteins with prot.id, prot.name, prot.seq, ...
    """
    if list_of_species is None:
        list_of_species = ['Anaerostipes_caccae']

    list_of_files = os.listdir("Proteom")
    proteins = list()

    for file_name in list_of_files:
        file_name = file_name[:-4]  # remove the ".faa" ending
        if file_name in list_of_species:
            with open("Proteom/" + file_name + ".faa") as handler:
                for prot in SeqIO.parse(handler, "fasta"):
                    proteins.append(prot)

    return proteins


def extend_graph(g):
    """
    function adds an output_Node node to every component
    which can be used as trash in the linear functions
    And an input_Node to every Node which represents Glucose or Co-Factors.
    :param g:
    :return: g_extended
    """
    out_node_id = 0
    in_node_id = 0
    new_nodes = dict()
    # find new nodes and save them in dict
    for node in g.nodes:
        # print("how does node variable look like? ", node)
        # print(f"## NODE {node} is: {g.nodes[node]}")
        if not g.nodes[node]['reaction']:
            out_name = "out_" + str(out_node_id)
            new_nodes[out_name] = (False, node)
            out_node_id += 1
        if node == "D-glucose" or node in cofactors:
            in_name = "in_" + str(in_node_id)
            new_nodes[in_name] = (True, node)
            in_node_id += 1

    # add nodes & edges
    for key in new_nodes.keys():
        if new_nodes[key][0]:  # input_node
            g.add_node(key, reaction=True, in_node=True, out_node=False)
            g.add_edge(u_of_edge=key, v_of_edge=new_nodes[key][1], attr=1)
        else:  # output_node
            g.add_node(key, reaction=True, in_node=False, out_node=True)
            g.add_edge(u_of_edge=new_nodes[key][1], v_of_edge=key, attr=1)

    return g


def create_prot_graph(graph, p):
    """
    Function extracts the Part of the graph which includes
    the AminoAcids in the Protein p. it extends a biomass_reaction
    node and a protein node. Also adds input & output nodes to graph.
    Output graph can be used to perform
    flux balance analysis on it.
    """

    # proteins = read_protein_data()
    # print(f"number of proteins: {len(proteins)}")

    *_, glucose_graph = bfs_hyper(graph, 'D-glucose')

    # create dictionary aa_amounts {Captial_Letter_AA: (fraction)}
    # fraction: Anteil der AA am Protein (#AA/len(prot)
    aa_amounts = {convert_letter(i): p.seq.count(i) for i in p.seq}
    am_ac_set = set(p.seq)
    am_ac_list = list()
    for a in am_ac_set:
        b = convert_letter(a)
        am_ac_list.append(b)

    # extract amino_subgraphs
    print("print aminos in union_subgraph :", am_ac_list)
    aa_union_subgraph = nx.reverse(get_amino_reqs_all(graph,
                                                      aminos=am_ac_list,
                                                      prnt=False))

    # intersect with glucose-graph
    prot_graph = build_amino_pathway(glucose_graph, aa_union_subgraph)

    # add in- and output nodes
    prot_graph_extend = extend_graph(prot_graph)

    # add a final Reaction node R_b
    # (Reactionsknoten = Proteinkonten hinzufuegen ( = reaction_output)
    # Reaction_input sind die Aminosaeuren aus denen Protein besteht.
    # Kanten-Gewichte an den Inputs: fractions)
    prot_graph_extend.add_node("biomass_reaction", reaction=True,
                               in_node=False, out_node=False)
    prot_graph_extend.add_node("protein", reaction=False,
                               in_node=False, out_node=False)
    prot_graph_extend.add_edge(u_of_edge="biomass_reaction",
                               v_of_edge="protein", attr=1)
    for amino in aa_amounts:
        if amino in prot_graph_extend:
            prot_graph_extend.add_edge(u_of_edge=amino,
                                       v_of_edge="biomass_reaction",
                                       attr=aa_amounts[amino])

    return prot_graph_extend, aa_amounts


def model_fba(graph):
    # read protein data

    proteins = read_protein_data()
    prot = proteins[0]
    # extend graph and get AAs in prot
    prot_graph_extend, aa_amounts = create_prot_graph(graph, prot)

    model = LpProblem("Protein_assembling", LpMaximize)

    # create variables

    variable_dict = {}

    for j in prot_graph_extend.nodes:
        if prot_graph_extend.nodes[j]['reaction'] and not j.startswith("in_"):
            variable_dict[j] = pulp.LpVariable(str(j), lowBound=0, upBound=100,
                                               cat=LpContinuous)
        elif prot_graph_extend.nodes[j]['reaction'] \
                and j.startswith("in_") and j != 'D-glucose':
            variable_dict[j] = pulp.LpVariable(str(j), lowBound=0, upBound=0,
                                               cat=LpContinuous)
        elif prot_graph_extend.nodes[j]['reaction'] \
                and j.startswith("in_") and j == 'D-glucose':
            variable_dict[j] = pulp.LpVariable(str(j),
                                               lowBound=0, upBound=4,
                                               cat=LpContinuous)
        """ 
        if prot_graph_extend.nodes[j]['reaction'] and (j == "D-glucose" 
        or j in cofactors):
            variable_dict[j] = pulp.LpVariable(str(j), 
            lowBound=1000, upBound=1000, cat=LpContinuous)
        """
    # erklaerung: input_nodes. die in variable_dict packen mit lB = uB=1000

    model += pulp.lpSum(variable_dict), "Profit"

    # create constraints
    # constraints ueber AAs
    for h in prot_graph_extend.nodes:
        aa_in_constraint_list = []
        aa_out_constraint_list = []
    # amino constraints
        if h in aa_amounts.keys():
            for i in prot_graph_extend.predecessors(h):
                aa_in_constraint_list += \
                    prot_graph_extend.edges[i, h]["attr"] * variable_dict.get(i)
    #  print("aa_in_constraint_list : ", aa_in_constraint_list)
            for j in prot_graph_extend.successors(h):
                aa_out_constraint_list \
                    += prot_graph_extend.edges[h, j]["attr"] * variable_dict[j]
            model += (1/aa_amounts.get(h)) * (pulp.lpSum(aa_in_constraint_list) - pulp.lpSum(aa_out_constraint_list)) <= variable_dict.get('biomass_reaction')

    # constraint ueber die anderen Components
    for k in prot_graph_extend.nodes:
        out_constraint_list = []
        in_constraint_list = []
    #    print("Knoten in prot_graph extend : ", k)
        if not prot_graph_extend.nodes[k]['reaction'] \
                and k != 'D-glucose' and k != 'protein' and k not in aa_amounts:
            for n in prot_graph_extend.predecessors(k):
                in_constraint_list += \
                    variable_dict.get(n) * prot_graph_extend.edges[n, k]["attr"]
            for m in prot_graph_extend.successors(k):
                out_constraint_list += \
                    variable_dict.get(m) * prot_graph_extend.edges[k, m]["attr"]
            model += pulp.lpSum(in_constraint_list) >= pulp.lpSum(out_constraint_list)

    # glucose constraints
    for p in prot_graph_extend.nodes:
        g_out_constraint_list = []
        g_in_constraint_list = []
        if p == 'D-glucose':
            for q in prot_graph_extend.successors(p):
                g_out_constraint_list += \
                    variable_dict.get(q) * prot_graph_extend.edges[p, q]["attr"]
            for r in prot_graph_extend.predecessors(p):
                g_in_constraint_list += \
                    variable_dict.get(r) * prot_graph_extend.edges[r, p]["attr"]
            model += pulp.lpSum(g_in_constraint_list) >= pulp.lpSum(g_out_constraint_list)

    # solve the model

    model.solve()

    # Print the solution of the model
    value_dict = {}
    for v in model.variables():
        value_dict[v.name] = v.varValue

    return value_dict
