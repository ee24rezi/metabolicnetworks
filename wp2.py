from functions_for_wp2 import *


def wp2(species: list = None):

    if not species or len(species) > 1:
        species = ['eramosum_cimIV']
        if len(species) > 1:
            print("Please use only one species!")

    data_dict = read_sihumix_data(species)
    graph = build_graph(data_dict)
    model_fba(graph)
    # prepare flux analyse for first protein:
    proteins = read_protein_data()
    print(f"Number of proteins: {len(proteins)} for species: {species}")

    prot_graph, aminos = create_prot_graph(graph, proteins[0])
    # plot graph in results/graphs as pdf
    print_graph(prot_graph, with_all_substs=False,
                title=f"WP2 extended graph ecoli_adam")
    print("nodes : ", prot_graph.nodes)
