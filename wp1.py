from networkx import DiGraph
from functions_for_wp1 import *
from compounds import amino_acid_list as aa_list
import pandas as pd
import matplotlib.pyplot as plt
from test_dict import test_dict


def wp1():
    print("Which organism can produce which AAs?")
    list_of_species = [s for s in os.listdir("sihumix") if "." not in s]

    stats_dict_nodes = dict()
    stats_dict_edges = dict()
    results_nodes = dict()
    results_edges = dict()

    for species in list_of_species:
        print(f"\n\n#################\n* {species} *\n#################\n")

        data_dict = read_sihumix_data(list_of_species=[species])
        graph: DiGraph = build_graph(data_dict)
        found, vis_rs, vis_substs, subgraph = bfs_hyper(graph, "D-glucose")

        ###############
        # Amino Acids #
        ###############
        producable_aas = list()
        missing_aas = list()
        for aa in aa_list:
            if aa in found:
                producable_aas.append(aa)
            else:
                missing_aas.append(aa)

        print(f"can produce: ")
        print(*producable_aas, sep=",   ")
        print(f"can NOT produce: ")
        print(*missing_aas, sep=",   ")

        ###############
        # Whole Graph #
        ###############
        whole_graph_nn = nx.number_of_nodes(graph)
        whole_graph_ne = nx.number_of_edges(graph)
        print(f"""\n\tThe whole Graph has {whole_graph_nn} nodes and {
        whole_graph_ne} edges""")
        results_nodes["Whole Graph"] = whole_graph_nn
        results_edges["Whole Graph"] = whole_graph_ne

        ###################
        # D-glucose Graph #
        ###################
        subgraph_nn = nx.number_of_nodes(subgraph)
        subgraph_ne = nx.number_of_edges(subgraph)
        print(f"""\n\tThe D-glucose subgraph has {whole_graph_nn} nodes and {
        whole_graph_ne} edges""")
        results_nodes["Glucose"] = subgraph_nn
        results_edges["Glucose"] = subgraph_ne

        #####################
        # Amino-Acid Graphs #
        #####################
        aa_union_subgraph = nx.reverse(get_amino_reqs_all(graph, prnt=False))

        aa_unified_graph_nn = nx.number_of_nodes(aa_union_subgraph)
        aa_unified_graph_ne = nx.number_of_edges(aa_union_subgraph)
        print(f"""\n\tThe Amino-Acid Unified-Graph has {
        aa_unified_graph_nn} nodes and {
        aa_unified_graph_ne} edges""")
        results_nodes["AA union"] = aa_unified_graph_nn
        results_edges["AA union"] = aa_unified_graph_ne

        #######################
        # Amino-Pathway Graph #
        #######################
        amino_pathway = build_amino_pathway(subgraph, aa_union_subgraph)

        aa_pathway_graph_nn = nx.number_of_nodes(amino_pathway)
        aa_pathway_graph_ne = nx.number_of_edges(amino_pathway)
        print(f"""\n\tThe amino_pathway Graph has {
        aa_pathway_graph_nn} nodes and {
        aa_pathway_graph_ne} edges""")
        results_nodes["AA-Pathway"] = aa_pathway_graph_nn
        results_edges["AA-Pathway"] = aa_pathway_graph_ne

        #######
        # add #
        #######
        stats_dict_nodes[species] = results_nodes
        results_nodes = {}
        stats_dict_edges[species] = results_edges
        results_edges = {}

    # create plots
    df = pd.DataFrame(stats_dict_nodes)
    print(df)
    df.to_csv("results/stats_dict_nodes.csv")
    df = pd.DataFrame(stats_dict_edges)
    print(df)
    df.to_csv("results/stats_dict_edges.csv")


def plot_results_wp1():
    df = pd.read_csv("results/stats_dict_nodes.csv", index_col=0)
    # print(df)
    df.plot.bar(rot=0, legend=False)
    plt.subplots_adjust(left=0.2, bottom=0.3)
    plt.ylabel(f"Number of nodes")
    plt.xticks(rotation=45)
    plt.title('Size of (Sub-)Graphs')
    plt.savefig('results/graphsize_nodes.pdf')
    # plt.show()

    df = pd.read_csv("results/stats_dict_edges.csv", index_col=0)
    # print(df)
    df.plot.bar(rot=0, legend=False)
    plt.subplots_adjust(left=0.2, bottom=0.3)
    plt.ylabel(f"Number of edges")
    plt.xticks(rotation=45)
    plt.title('Size of (Sub-)Graphs')
    plt.savefig('results/graphsize_edges.pdf')
    # plt.show()

    # print legend (with some hack)
    df = pd.read_csv("results/legend.csv", index_col=0)
    df.plot.bar(rot=0)
    plt.savefig('results/legend.pdf')
    # plt.show()

    print("\nSee the graphsize-results as bar plots in 'results' folder!\n")


def plot_wp1_graphs_with_small_test_dict():
    print("\nUse the following test data:")
    for key in test_dict.keys():
        print(key, test_dict[key])

    print("\nBuild the test_graph")
    test_graph = build_graph(test_dict)
    print("\nPlot the test_graph as pdf in 'results/graphs' folder")
    print_graph(test_graph, with_all_substs=False,
                title="WP1 Whole Test-Graph")

    found, visited_rctns, visited_substs, subgraph = bfs_hyper(test_graph,
                                                               "D-glucose")
    print(" --> result of bfs_hyper:\n", found)
    print(" --> visited_rctns:\n", visited_rctns)
    print(" --> visited_substs:\n", visited_substs)

    print("..plot subgraph (of D-glucose) as pdf")
    print_graph(subgraph, with_all_substs=False, title="WP1 Glucose subgraph")

    print("\nReverse search amino acids and plot unified graph as pdf")
    aa_union_subgraph = nx.reverse(get_amino_reqs_all(test_graph, prnt=False))
    print_graph(aa_union_subgraph, with_all_substs=False,
                title="WP1 Unified Amino Acid Subgraph")

    print("Extract all reactions (with subgraph cut unified subgraph)")
    amino_pathway = build_amino_pathway(subgraph, aa_union_subgraph)
    print_graph(amino_pathway, with_all_substs=False,
                title="WP1 Reactions aka Amino Pathway")
