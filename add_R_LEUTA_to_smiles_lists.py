import os

list_of_folders = os.listdir("sihumix")

stringlist = ["\n",
              "Bigg ID: R_LEUTA MetaNetXId: MNXR192685 Reversible: True\n",
              "ECs: 2.6.1.42;2.6.1.6;2.6.1.67\n",
              "L-leucine + 2-oxoglutarate = 4-methyl-2-oxopentanoate + L-glutamate\n",
              "CC(C)C[CSyntax error: H](C(=O)O)N.O=C([O-])CCC(=O)C(=O)[O-]>>CC(C)CC(=O)C(=O)[O-].[NH3+][CH](CCC(=O)[O-])C(=O)[O-]\n"]

for folder in list_of_folders:
    if "." not in folder:
        smiles_list_file = open(
            "sihumix/" + folder + "/" + folder + ".smiles_list", 'a')
        smiles_list_file.writelines(stringlist)
        smiles_list_file.close()
