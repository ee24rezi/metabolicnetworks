from functions_for_wp1 import *
from functions_for_wp2 import extend_graph, read_protein_data, create_prot_graph
from test_dict import test_dict, prot_dict

####################################
# first test with small test graph #
####################################
test_graph = build_graph(test_dict)
for node in test_graph.nodes:
    print(f"--- test_graph ## NODE {node} is: {test_graph.nodes[node]}")

*_, subgraph = bfs_hyper(test_graph, "D-glucose")
print_graph(subgraph, with_all_substs=False, pdf=False, title="subgraph")

print("reverse search amino acids and print unified graph")

aa_union_subgraph = nx.reverse(get_amino_reqs_all(test_graph, prnt=False))
print_graph(aa_union_subgraph, with_all_substs=False, pdf=False,
            title="aa_union_subgraph")

print("Extract all reactions (subgraph \\cap aa_union_subgraph)")
amino_pathway = build_amino_pathway(subgraph, aa_union_subgraph)
for node in amino_pathway.nodes:
    print(f"### amino_pathway ## NODE {node} is: {amino_pathway.nodes[node]}")
print_graph(amino_pathway, with_all_substs=False, pdf=False,
            title="amino_pathway")
print("now we enter wp2")
extended_g = extend_graph(amino_pathway)
print_graph(extended_g, with_all_substs=True, pdf=False,
            title="extended input and output node")


#####################################
# second test with small prot graph #
# containing all amino acids        #
# plots in results/graphs as pdf    #
#####################################

prot_test_graph = build_graph(prot_dict)

*_, subgraph = bfs_hyper(prot_test_graph, "D-glucose")
print_graph(subgraph, with_all_substs=False, title="subgraph")

# print("reverse search amino acids and print unified graph")

aa_union_subgraph = nx.reverse(get_amino_reqs_all(prot_test_graph, prnt=False))
print_graph(aa_union_subgraph, with_all_substs=False, title="aa_union_subgraph")

# print("Extract all reactions (subgraph \\cap aa_union_subgraph)")
amino_pathway = build_amino_pathway(subgraph, aa_union_subgraph)
for node in amino_pathway.nodes:
    print(f"### amino_pathway ## NODE {node} is: {amino_pathway.nodes[node]}")
print_graph(amino_pathway, with_all_substs=True, title="amino_pathway")

print("now we enter wp2")
extended_g = extend_graph(amino_pathway)
print_graph(extended_g, with_all_substs=True,
            title="extended input and output node")
print("nodes in resulting graph: ", extended_g.nodes())
print("edges: ", extended_g.edges())
print("node reaction attributse: ",
      nx.get_node_attributes(extended_g, "reaction"))
print("edge weights: ", nx.get_edge_attributes(extended_g, "attr"))

#
# Prot Data
#
p_data = read_protein_data()
count = 0
prot_graph = nx.DiGraph()
for p in p_data:
    prot_graph, aa_amounts = create_prot_graph(prot_test_graph, p)
    if count == 0:
        break

print_graph(prot_graph, with_all_substs=False, title="prot graph small")
print_graph(prot_graph, with_all_substs=True, title="prot graph")
