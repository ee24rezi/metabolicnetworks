amino_acid_list = ["L-arginine", "L-valine", "L-methionine", "L-glutamate",
                   "L-glutamine", "L-tyrosine", "L-tryptophan",
                   "L-proline", "L-cysteine", "L-histidine",
                   "L-asparagine", "L-aspartate", "L-phenylalanine",
                   "L-threonine", "L-lysine", "L-serine", "L-isoleucine",
                   "glycine", "L-alanine", "L-leucine"]

amino_acid_dict = {
    "R": {"name": "L-arginine"},
    "V": {"name": "L-valine"},
    "M": {"name": "L-methionine"},
    "E": {"name": "L-glutamate"},
    "Q": {"name": "L-glutamine"},
    "Y": {"name": "L-tyrosine"},
    "W": {"name": "L-tryptophan"},
    "P": {"name": "L-proline"},
    "C": {"name": "L-cysteine"},
    "H": {"name": "L-histidine"},
    "N": {"name": "L-asparagine"},
    "D": {"name": "L-aspartate"},
    "F": {"name": "L-phenylalanine"},
    "T": {"name": "L-threonine"},
    "K": {"name": "L-lysine"},
    "S": {"name": "L-serine"},
    "I": {"name": "L-isoleucine"},
    "G": {"name": "glycine"},
    "A": {"name": "L-alanine"},
    "L": {"name": "L-leucine"}
}


cofactors_old = ["AMP", "ADP", "ATP", "NAD(+)", "NADH", "NADP(+)", "NADPH",
             "CTP", "CoA", "H2O", "NH4(+)", "hydrogensulfide", "H(+)"]

cofactors = ["AMP", "ADP", "ATP", "GDP", "NAD(+)", "NADH", "NADP(+)", "H(+)",
                  "NADPH", "FAD", "FADH2", "heme b", "UTP", "CTP", "CoA", "FMN",
                  "H2O", "NH4(+)", "hydrogen sulfide", "CO2", "phosphate"]


all_species = ['bproducta_cimIV',
               'eramosum_cimIV',
               'ecoli_cimIV',
               'lplantarum_cimIV',
               'cbuty_cimIV',
               'cbuty_adam',
               'eramosum_adam',
               'acacae_cimIV',
               'btheta_adam',
               'ecoli_adam',
               'btheta_cimIV',
               'blongum_cimIV',
               'bproducta_adam',
               'acacae_adam',
               'blongum_adam',
               'lplantarum_adam']
