import networkx as nx
from functions_for_wp1 import read_sihumix_data, build_graph
from compounds import all_species

# generate atom transition networks for each species and medium
# medium: medium in which an organism can be cultivated

# build up atn for example species
d = read_sihumix_data(list_of_species=all_species[0])
g = build_graph(d)

# get some stats from it
# Achtung: Wir haben Reaction Knoten und auch reverse_Reaction_Nodes
# number of components = all nodes != reactions ?

reactions = nx.get_node_attributes(g, 'reaction')
num_comp = sum(1 for v in reactions.values() if not v)

# component size:  what do they mean ?

# Density ( depents on number of nodes and number of edges in g)
# -> how do we want to calculate it,
# what does ist mean to us? including reaction nodes ? including reverse nodes ?


# path / cycle length
# shortest_path_length & shortest_cycle_length
# path from where to where ?  whats interesting?
# cycles, also: whats interesting..

# cycles = nx.find_cycle(g)
print("number of nodes total (reactions, reactions_R, components: ",
      g.number_of_nodes())
print("number of components: ", num_comp)
print("number of reactions (reverse included): ",
      (g.number_of_nodes()-num_comp))
print("number of edges", g.number_of_edges())
print("density: ", nx.density(g))
# print("sp_len: ", sp_len)
# print("cycles" , type(cycles))
