import wp1
import wp2

############################
# RUN WP1 with all species #
############################

wp1.wp1()
# results will be printed as well as saved in results/stats_dict_x.csv
# plot the results:
wp1.plot_results_wp1()

# plot the graphs for some sample data (reactions are made up)
wp1.plot_wp1_graphs_with_small_test_dict()

############################
# RUN WP2 for some species #
############################

wp2.wp2(species=['eramosum_cimIV'])

print("Finished")
print("All plots can be found in the 'results' folder as PDFs.")
