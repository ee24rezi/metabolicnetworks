from functions_for_wp1 import *

test_dict = {"MNXR188277": (False,
                            "H(+) + ATP = quinolinate",
                            "smiles-str"),
             "MNXR173045": (True,
                            "ATP + H2O = L-glutamate",
                            "smiles-str"),
             "MNXR000002": (False,
                            "H(+) + ATP + H2O + H2O + D-glucose = L-serine",
                            "smiles-str"),
             "MNXR19073915": (False,
                              "D-glucose + H2O + ATP = L-lysine",
                              "smiles-str")
             }

print("build and print test_graph")
test_graph = build_graph(test_dict)
print_graph(test_graph, title="Whole Graph", pdf=False)

print("get and print subgraph (of D-glucose)")
*_, subgraph = bfs_hyper(test_graph, "D-glucose")
print_graph(subgraph, title="subgraph", pdf=False)

print("reverse search amino acids and print unified graph")
aa_union_subgraph = nx.reverse(get_amino_reqs_all(test_graph, prnt=True))
print_graph(aa_union_subgraph, title="aa_union_subgraph", pdf=False)
