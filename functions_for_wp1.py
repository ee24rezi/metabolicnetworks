import os
import networkx as nx
import queue
from compounds import cofactors, amino_acid_list
import matplotlib.pyplot as plt


def read_sihumix_data(list_of_species=None) -> dict:
    """
    function to read from sihumix data (folder)

    :return: dict {MetaNetXId: (reversible, reaction_string, smiles_sting)}
    """
    if list_of_species is None:
        list_of_species = ['ecoli_adam']

    data_dict = dict()

    list_of_folders = os.listdir("sihumix")
    metanet_key = ""
    reversible = bool()
    reaction_string = ""

    for folder in list_of_folders:
        if "." not in folder and folder in list_of_species:
            smiles_list_file = open(
                "sihumix/" + folder + "/" + folder + ".smiles_list")
            line_count = 5  # to distinguish lines
            for line in smiles_list_file.readlines():
                line.strip("\n")  # remove line break
                line_count += 1
                if "MetaNetXId: " in line and " Reversible: " in line \
                        and "EMPTY" not in line:
                    substr = line.split("MetaNetXId: ")[1]
                    metanet_key, rev_string = substr.split(" Reversible: ")
                    reversible = False if "False" in rev_string else True
                    line_count = 1
                    # emtpy the strings (just in case)
                    reaction_string, smiles_string = "", ""
                elif line_count == 3:
                    reaction_string = line.strip("\n")
                elif line_count == 4:
                    smiles_string = line.strip("\n")
                    # add the data
                    data_dict[metanet_key] = (reversible,
                                              reaction_string,
                                              smiles_string)
    return data_dict


def build_graph(d, with_standard_substs=True):
    """
    dict {metanetxid: (reversible,reaction_string,smiles_string)}
    function builds up a digraph from a dictionary
    :param with_standard_substs: if True then standard substances are added
            as nodes into the graph (otherwise only nodes from d will be added)
    :param d: data dictionary
    :return g: nx.DiGraph
    """
    g = nx.DiGraph()

    # add standard substances
    if with_standard_substs:
        for subst in cofactors + amino_acid_list:
            g.add_node(subst, reaction=False, in_node=False, out_node=False)

    # extract substance nodes and edge_dict
    edge_dict = dict()
    edge_id = 0
    substances_all = []

    for key in d.keys():
        # note:
        # substances_set collects all substances.
        # educts / products list & set need to be new in every iteration.
        reaction_string = d[key][1]
        if "=" in reaction_string:
            # substance vertices:
            educts, products = reaction_string.split(" = ")
            educts_list = educts.split(" + ")
            products_list = products.split(" + ")
            substances = educts_list + products_list
            # add substances to subst-list
            substances_all += substances
            # add educts, products to sets, to avoid duplicates
            educts_set = set(educts_list)
            products_set = set(products_list)

            # build edges in edge_dict
            # edge_dict {edge_id: (metanetxid, substance, type (educt/product),
            #                           num (number of elements), reversible}

            for educt in educts_set:
                edge_dict[edge_id] = (key, educt, "to_r",
                                      educts_list.count(educt), d[key][0])
                edge_id += 1
            for product in products_set:
                edge_dict[edge_id] = (key, product, "from_r",
                                      products_list.count(product), d[key][0])
                edge_id += 1

    # add reactions and substance nodes to g
    substances_set = set(substances_all)
    for node in substances_set:
        g.add_node(node, reaction=False, in_node=False, out_node=False)
    for key in d.keys():
        g.add_node(key, reaction=True, in_node=False, out_node=False)
        if d[key][0]:  # reverse
            id_reverse = key + "_R"
            g.add_node(id_reverse, reaction=True, in_node=False, out_node=False)

    # add edges to g
    for key in edge_dict.keys():
        if edge_dict[key][2] == "to_r":
            g.add_edge(u_of_edge=edge_dict[key][1],
                       v_of_edge=edge_dict[key][0],
                       attr=edge_dict[key][3])
            if edge_dict[key][4]:  # reverse True
                g.add_edge(u_of_edge=edge_dict[key][0] + "_R",
                           v_of_edge=edge_dict[key][1],
                           attr=edge_dict[key][3])
        if edge_dict[key][2] == "from_r":
            g.add_edge(u_of_edge=edge_dict[key][0],
                       v_of_edge=edge_dict[key][1],
                       attr=edge_dict[key][3])

            if edge_dict[key][4]:  # reverse True
                g.add_edge(edge_dict[key][1],
                           edge_dict[key][0] + "_R",
                           attr=edge_dict[key][3])
    return g


def bfs_hyper(graph: nx.DiGraph, root_compound: str = None,
              search_compounds: list = None) -> (list, set, set, nx.DiGraph):
    """
    :type graph: input graph
    :param graph: the DiGraph to be searched
    :param root_compound: the start node of the search
    :param search_compounds: list of compounds to be searched for
           (default is all amino acids)
    :return (found_compounds, visited_reactions, visited_substances, subgraph)
    """
    if root_compound is None:
        root_compound = "D-glucose"
    if search_compounds is None:
        search_compounds = ["all"]

    # print("search", search_compounds, "from", root_compound, "in", graph)

    found_compounds = list()
    visited_rs_count = dict()
    visited_substances = set(cofactors + [root_compound])

    q = queue.Queue()
    # insert all visited substances to queue
    for s in visited_substances:
        q.put(s)

    while not q.empty():
        current_node = q.get()
        if not graph.has_node(current_node):
            continue  # node not contained -> skip it
        # get all reaction aka hyper edges aka R-nodes of current node cn
        reactions = [r for r in graph.neighbors(current_node)]

        for r_node in reactions:  # test whether all inputs for r_node exist
            # increase counter of r_node (if not exists insert with 1)
            visited_rs_count[r_node] = visited_rs_count.get(r_node, 0) + 1
            # get all educts required for reaction
            req_educts = [edge[0] for edge in graph.in_edges(r_node)]
            if set(req_educts).issubset(visited_substances):
                # add new products of the reaction to visited and q
                for product in graph.neighbors(r_node):
                    if product in visited_substances:
                        continue  # product is already known
                    visited_substances.add(product)
                    if product in search_compounds or "all" in search_compounds:
                        found_compounds.append(product)  # found an amino acid
                    if set(search_compounds).issubset(found_compounds):
                        return found_compounds  # found all search_compounds
                    q.put(product)

    # build subgraph
    visited_reactions = set(visited_rs_count.keys())
    all_nodes = visited_substances.union(visited_reactions)
    subgraph = nx.induced_subgraph(graph, all_nodes)

    return found_compounds, visited_reactions, visited_substances, subgraph


def print_graph(graph: nx.DiGraph,
                with_all_substs=True, title="graph", pdf=True):
    """
    easy method to draw our graphs
    :graph nx.DiGraph: the graph to be plotted
    :with_all_substs bool: if False, then only non-default-nodes are plotted
    :title str: the title of the matplot plot
    """
    fig, ax = plt.subplots(figsize=(12, 12))

    if not with_all_substs:
        to_remove = cofactors.copy()
        for i in range(100):
            to_remove.append(f"out_{i}")
            to_remove.append(f"in_{i}")
        subst_nodes = set(graph.nodes).difference(to_remove)
        graph = nx.induced_subgraph(graph, subst_nodes)

    positions = nx.spring_layout(graph)
    if nx.is_planar(graph):
        nx.draw(graph, pos=nx.planar_layout(graph), node_color="red",
                with_labels=True)
    else:
        nx.draw(graph, pos=positions, node_size=2, alpha=0.3, edge_color="r",
                font_size=8, with_labels=True)

    # add title and do show result
    font = {"fontname": "Helvetica",
            "color": "r",
            "fontweight": "bold",
            "fontsize": 25}
    ax.set_title(title, font)
    ax.margins(0.1, 0.05)
    fig.tight_layout()
    plt.axis("off")
    if pdf:
        plt.savefig(f"results/graphs/{title.replace(' ', '_')}.pdf")
    else:
        plt.show()


def get_amino_reqs_all(graph, aminos: list = None, prnt=False) -> nx.DiGraph:
    """
    invokes a bfs_hyper search from all substances in aminos (backward search)
    to retrieve sub-graphs that are then united and returned
    :aminos list: a list of substances (all amino acids by default)
    :prnt bool: print all the sub-graphs (False by default)
    :return nx.DiGraph: the union of all sub-graphs (amino backward searches)
    """
    if aminos is None:
        aminos = amino_acid_list

    aa_node_union = set()
    reverse_graph = nx.reverse(graph)

    for aa in aminos:
        # get amino_subgraph
        _, v_rs, v_substs, amino_subgraph = bfs_hyper(reverse_graph, aa)
        all_nodes = v_substs.union(v_rs)
        if len(all_nodes) > 0:
            if prnt:  # only print sub-graph when prnt is set True
                print_graph(nx.reverse(amino_subgraph), # with_all_substs=False,
                            title=f"amino acid subgraph of {aa}", pdf=False)
            aa_node_union = aa_node_union.union(all_nodes)
    # get the whole subgraph
    # (has to be done on the reverse graph for some reason.. [see testcase])
    aa_union_subgraph = nx.induced_subgraph(reverse_graph, aa_node_union)

    return aa_union_subgraph


def build_amino_pathway(g, v):

    """
    function intersects two graphs.
    :param g: graph 1
    :param v: graph 2
    :return: graph i which contains n e N_g and e N_v and Edges e E_g and E_v
    """
    # nodes
    i = nx.DiGraph()
    for node in g.nodes():
        if node in v.nodes():
            # print("knoten:", node)
            i.add_node(node, reaction=g.nodes[node]['reaction'],
                       in_node=g.nodes[node]['in_node'],
                       out_node=g.nodes[node]['out_node'])
    # edges
    for edge in g.edges():
        if edge in v.edges():
            i.add_edge(u_of_edge=edge[0], v_of_edge=edge[1],
                       attr=g.edges[edge]['attr'])
    return i
